//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <stack>

	
using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE  {BOOL, INT, INTP, BOOLP};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string


map<string, TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPE Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	TYPE type;
	type=DeclaredVariables[lexer->YYText()];
	return type;
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	TYPE type;
	type=DeclaredVariables[lexer->YYText()];
	return type;
}
			// Called by Term() and calls Term()
TYPE Expression(void);

TYPE Factor(void){
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		TYPE type;
		type = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
		return type;
	}
	else
		if (current==NUMBER)
			Number();
	     	else
				if(current==ID)
					Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue");
		TYPE type = INT;			
		return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	OPMUL mulop;
	TYPE type1;
	type1 = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		TYPE type2;
		type2 = Factor();
		if(type1!=type2){
			Error("Meme type attendu");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout <<"\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE type1;
	type1 = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		TYPE type2;
		type2 = Term();
		if(type1!=type2){
			Error("Même type attendu");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type1;
}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
    stack<string> s;
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	cout<<"FormatString1:    .string \"%llu\\n\""<<endl;
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
    cout << lexer->YYText() << ":\t.quad 0"<<endl;
	s.push(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
        cout << lexer->YYText() << ":\t.quad 0"<<endl;
        s.push(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
    if(current!=ASSI){
        Error("caractère ':' attendu");
    }
    current=(TOKEN) lexer->yylex();
    if(current!=TEST){
        Error("le type était attendu ");
    }
    TYPE sauv;

    if(strcmp(lexer->YYText(),"INT")==0){
        sauv = INT;
    }
    if(strcmp(lexer->YYText(),"INTP")==0){
        sauv = INTP;
    }
    if(strcmp(lexer->YYText(),"BOOL")==0){
        sauv = BOOL;
    }
    if(strcmp(lexer->YYText(),"BOOLP")==0){
        sauv = BOOLP;
    }
    
    
    int taille = s.size();
    for(int i=0; i<taille; i++){
    
        DeclaredVariables.insert(make_pair(s.top(),sauv));
        s.pop();
       
    }
    current=(TOKEN) lexer->yylex();
        
}


void DeclarationPart2(void){
    stack<string> s;
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
    cout << lexer->YYText() << ":\t.quad 0"<<endl;
	s.push(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
        cout << lexer->YYText() << ":\t.quad 0"<<endl;
        s.push(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
    if(current!=ASSI){
        Error("caractère ':' attendu");
    }
    current=(TOKEN) lexer->yylex();
    if(current!=TEST){
        Error("le type était attendu ");
    }
    TYPE sauv;

    if(strcmp(lexer->YYText(),"INT")==0){
        sauv = INT;
    }
    if(strcmp(lexer->YYText(),"INTP")==0){
        sauv = INTP;
    }
    if(strcmp(lexer->YYText(),"BOOL")==0){
        sauv = BOOL;
    }
    if(strcmp(lexer->YYText(),"BOOL")==0){
        sauv = BOOLP;
    }
    
    
    for(int i=0; i<s.size(); i++){
        DeclaredVariables.insert(make_pair(s.top(),sauv));
        s.pop();
    }
    current=(TOKEN) lexer->yylex();
        
}


// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}
void IFStatement(void);
// Expression := SimpleExpression [RelationalOperator SimpleExpression]

TYPE Expression(void){
	OPREL oprel;
	TYPE type1;
	type1 = SimpleExpression();
	
	if(current==RELOP){
        
		oprel=RelationalOperator();
		TYPE type2;
		type2 = SimpleExpression();
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
    
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
              
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
              
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
                
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
             
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
               
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
        
	if(type1!=type2){
		Error("même type attendu");
	}
	else{
		TYPE type3;
		type3=BOOL;
        
		return type3;
	}
			
	}
	
	return type1;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
    TYPE type;
	if(current==ASSIGN){
        current=(TOKEN) lexer->yylex();
        TYPE type1;
        type1 = DeclaredVariables[variable];
        type = Expression();
        cout << "\tpop "<<variable<<endl;
        if(type!=type1){
            Error("Même type atteeendu");
        }
    }
    if(current==POINT){
        current=(TOKEN) lexer->yylex();
        TYPE type1;
        type1 = DeclaredVariables[variable];
        TYPE test;
        test = INTP;
        if((type1!=INTP)&&(type1!=BOOLP)){
            Error("Type pointeur attendu §§§§§");
        }
        
        type = Expression();
        if(((type==INT)&&(type1==BOOLP))||((type==BOOL)&&(type1==INTP))){
            Error("Mauvais type de pointeur");
        }
        cout << "\tpop %rax"<<endl;
        cout << "\tmovq %rax, %rsi"<<endl;
        cout << "\tpush %rsi"<<endl;
        cout << "\tpop "<<variable<<endl;
        }
    if(current==KEYWORD && strcmp(lexer->YYText(),"CIN")==0){
        int y;
        cin >> y;
        TYPE type = DeclaredVariables[variable];
        if(type!=INT){
            Error("Type int attendu");
        }

        cout <<"\tpush $"<<y<<endl;
        current=(TOKEN) lexer->yylex();
        cout << "\tpop "<<variable<<endl;
        
        
    }
        
        
	
    return(variable);
}

void Statement(void);

void IFStatement(void){
	unsigned int tagNbr = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(),"IF")==0){
		cout<< "IF"<<tagNbr<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		Expression();
		cout<<"\tpop %rax"<<endl;
		cout<<"\tcmpq $0, %rax"<<endl;
		cout<<"\tjz ELSE"<<tagNbr<<endl;
		if(current==KEYWORD && strcmp(lexer->YYText(),"THEN")==0){
			cout<< "THEN"<<tagNbr<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			Statement();
			cout<<"\tjp ENDIF"<<tagNbr<<endl;
			cout<< "ELSE"<<tagNbr<<":"<<endl;
			if(current==KEYWORD && strcmp(lexer->YYText(),"ELSE")==0){
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		}
		else{
			Error("caractères 'THEN' attendus");
			}
	}
	else{
		Error("caractères 'IF' attendus");
	}
	cout<<"ENDIF"<<tagNbr<<":"<<endl;
}

void WHILEStatement(void){
	unsigned int tagNbr = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(),"WHILE")==0){
		cout<< "WHILE"<<tagNbr<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		Expression();
        cout<<"\tpop %rax"<<endl;
        cout<<"\tcmpq $0, %rax"<<endl;
        cout<<"\tje ENDWHILE"<<tagNbr<<endl;
		if(current==KEYWORD && strcmp(lexer->YYText(),"DO")==0){
			cout<< "DO"<<tagNbr<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			Statement();
            cout<<"\tjmp WHILE"<<tagNbr<<endl;
		}
		cout<<"ENDWHILE"<<tagNbr<<":"<<endl;
	}
}

void FORStatement(void){
	unsigned int tagNbr = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(),"FOR")==0){
		cout<< "FOR"<<tagNbr<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		string var = AssignementStatement();
        string op;
        string hu;
		if((current==KEYWORD && strcmp(lexer->YYText(),"TO")==0) || (current==KEYWORD && strcmp(lexer->YYText(),"DOWNTO")==0)){
            if(current==KEYWORD && strcmp(lexer->YYText(),"TO")==0){
                cout<< "TO"<<tagNbr<<":"<<endl;
                op ="addq";
                hu="TO";
            }
            else{
                cout<< "DOWTO"<<tagNbr<<":"<<endl;
                op="subq";
                hu="DOWNTO";
            }
                
			current=(TOKEN) lexer->yylex();
			Expression();
            cout<<"\tpush "<<var<<endl;
            cout<<"\tpop %rax"<<endl;
            cout<<"\tpop %rbx"<<endl;
            cout<<"\tcmpq %rax,%rbx"<<endl;
            cout<<"\tje ENDFOR"<<tagNbr<<endl;
            
			if(current==KEYWORD && strcmp(lexer->YYText(),"DO")==0){
				cout<< "DO"<<tagNbr<<":"<<endl;
				current=(TOKEN) lexer->yylex();
				Statement();
                cout<<"\tpush "<<var<<endl;
                cout<<"\tpop %rax"<<endl;
                cout<<"\t"<<op<<" $1, %rax"<<endl;
                cout<<"\tpush %rax"<<endl;
                cout<<"\tpop "<<var<<endl;
                cout<<"\tjmp "<<hu<<tagNbr<<endl;
                
			}

		}
	}
	cout<<"ENDFOR"<<tagNbr<<":"<<endl;
}

void BlockStatement(void){
	unsigned int tagNbr = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(),"BEGIN")==0){	
		cout<< "BEGIN"<<tagNbr<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		Statement();
		current=(TOKEN) lexer->yylex();
		
		while(current==SEMICOLON){
			cout<< ";"<<endl;
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if(current==KEYWORD && strcmp(lexer->YYText(),"END")==0){
			cout<< "END"<<tagNbr<<":"<<endl;
		}
		else{
			Error("caractères 'END' attendus");
		}	
	}
	else{
		Error("caractères 'BEGIN' attendus");
	}
}

void DisplayStatement(void){
	if(current==KEYWORD && strcmp(lexer->YYText(),"DISPLAY")==0){
		current=(TOKEN) lexer->yylex();
		TYPE type=Expression();
		if(type!=INT){
			Error("type int attendus");
		}
		cout<<"\tpop %rdx"<<endl;                    // The value to be displayed
		cout<<"\tmovq $FormatString1, %rsi"<<endl;   // %llu\n
		cout<<"\tmovl    $1, %edi"<<endl;
		cout<<"\tmovl    $0, %eax"<<endl;
		cout<<"\tcall    __printf_chk@PLT"<<endl;
	}
	else{
		Error("caractères 'DISPLAY' attendus");
	}
}
void listElementStatement();
void listLabelStatment();

string valeur;
int sauvTag;
void CaseStatment(void){
    unsigned int tagNbr = ++TagNumber;
    sauvTag = TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(),"CASE")==0){
        cout<< "SWITCHCASE"<<tagNbr<<":"<<endl;
		current=(TOKEN) lexer->yylex();
        valeur=lexer->YYText();
		Expression();
		if(current==KEYWORD && strcmp(lexer->YYText(),"OF")==0){
            cout<< "OF"<<tagNbr<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			listElementStatement();
            while(current==KEYWORD && strcmp(lexer->YYText(),"CASE")==0){

                listElementStatement();
            }
            
            if(current==KEYWORD && strcmp(lexer->YYText(),"END")==0){
                cout<< "END"<<tagNbr<<":"<<endl;
                current=(TOKEN) lexer->yylex();
            }
            else{
                Error("End attendu");
            }
        }
        else{
            Error("OF attendu");
        }
    }
    else{
        Error("CASE attendu");
    }
}
string valeurcase;
void listElementStatement(void){
    unsigned int tagNbr = ++TagNumber;  
    cout<< "CASE"<<tagNbr<<":"<<endl;
    listLabelStatment();
    cout<<"\tpush "<<valeur<<endl;
    cout<<"\tpush "<<valeurcase<<endl;
    cout<<"\tpop %rax"<<endl;
    cout<<"\tpop %rbx"<<endl;
    cout<<"\tcmpq %rax, %rbx"<<endl;
    cout<<"\tjbe ENDCASE"<<tagNbr<<endl;
    current=(TOKEN) lexer->yylex();
    if(current==KEYWORD && strcmp(lexer->YYText(),"DO")==0){
        
        cout<< "DO"<<tagNbr<<":"<<endl;
        current=(TOKEN) lexer->yylex();
        Statement();
        cout<<"\tjmp END"<<sauvTag<<endl;
        cout<< "ENDCASE"<<tagNbr<<":"<<endl;
    }
}

void listLabelStatment(void){
    current=(TOKEN) lexer->yylex();
    valeurcase = lexer->YYText();
    
}




// Statement := AssignementStatement
void Statement(void){
	if(current==ID){
		AssignementStatement();
	}
	else if(current==RBRACKET){
        DeclarationPart2();
    }
    else if(current==KEYWORD && strcmp(lexer->YYText(),"CASE")==0){
        CaseStatment();
    }
	else if(current==KEYWORD && strcmp(lexer->YYText(),"IF")==0){
		IFStatement();
	}
	else if(current==KEYWORD && strcmp(lexer->YYText(),"WHILE")==0){
		WHILEStatement();
	}
	else if(current==KEYWORD && strcmp(lexer->YYText(),"FOR")==0){
		FORStatement();
	}
	else if(current==KEYWORD && strcmp(lexer->YYText(),"BEGIN")==0){
		BlockStatement();
	}
	else if(current==KEYWORD && strcmp(lexer->YYText(),"DISPLAY")==0){
		DisplayStatement();
	}

}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}



// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
//z:=(8==3)||(4==2*2);
//b:=(5/65+2)<(7%5).
	





