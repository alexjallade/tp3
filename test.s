			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:    .string "%llu\n"
a:	.quad 0
b:	.quad 0
c:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $0
	pop a
r:	.quad 0
	push a
	pop %rax
	movq %rax, %rsi
	push %rsi
	pop r
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
